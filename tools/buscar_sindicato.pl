#!/usr/bin/perl

use strict;
use utf8;

use Data::Dumper;
use REST::Google;
use JSON;

my $QUERY;
my $JSON_FILE;
my $JSON_FILE_OUTPUT = "/tmp/sindicato_secretario_sitios.json";
my $json_ref;

$SIG{INT} = \&write_file_and_die;

sub write_file_and_die
{

	open my $fh, "> $JSON_FILE_OUTPUT ";
	print $fh encode_json($json_ref);
	close $fh;

	print "Escribiendo archivo!\n";
	exit; 
}
 
foreach(@ARGV) {

	/^--json-file=(\S+)$/ and do {

		my ($arg, $jf) = split("=", $_);
		$JSON_FILE = $jf;

		next;
	};
}


REST::Google->service('http://ajax.googleapis.com/ajax/services/search/web');
REST::Google->http_referer('http://example.com');


if($JSON_FILE) {

	my $json;
	{
		local $/; #Enable 'slurp' mode
		open my $fh, "< $JSON_FILE" or die "$JSON_FILE No se encuentra";
		$json = <$fh>;
		close $fh;
	}

	$json_ref = decode_json($json);
	my $cant_sindicatos = scalar(@{$json_ref->{'sindicatos'}});
	my $k = 1;

	foreach my $sindicato(@{$json_ref->{'sindicatos'}}) {
		my $nombre_sindicato = $sindicato->{'nombre_sindicato'};

		my @response = search($nombre_sindicato);

		my @sites;
		foreach my $each(@response) {

			#print "TITLE: $each->{'title'}\n";
			#print "CONTENT: $each->{'content'}\n";
			#print "URL: $each->{'url'}\n";
			#print "\n";

			push(@sites, $each->{'url'});
		}

		$sindicato->{'sitios'}= \@sites;

		print "----------------------------------------------------------\n";
		print "$nombre_sindicato\n";
		print "$k/$cant_sindicatos\n\n";
		#print encode_json($sindicato);
		print Dumper($sindicato);

		$k++;
		sleep(3);

	}

	

} else {
	my $QUERY = $ARGV[0];
	die "No se suministro nombre de sindicato" if(!$QUERY);

	my @response = search($QUERY);

	foreach my $each(@response) {

		print "TITLE: $each->{'title'}\n";
		print "CONTENT: $each->{'content'}\n";
		print "URL: $each->{'url'}\n";
		print "\n";
	}

}

write_file_and_die();








sub search
{

	my $query = shift;

	my $res = REST::Google->new(q => $query,
								hl => "es",
	);

	if($res->responseStatus != 200) {

		 print("$query fallo, sigo\n");
	}

	my $data = $res->responseData;

	#print Dumper($data->{results});
	#exit;

	my @results;

	foreach my $res(@{$data->{results}}) {

		my %hash;
		next if(is_wikipedia($res->{'visibleUrl'}));

		$hash{'title'} = $res->{'titleNoFormatting'};
		$hash{'content'} = $res->{'content'};
		$hash{'url'} = $res->{'url'};


		push(@results, \%hash);
	}

	return @results;
}

sub is_wikipedia
{
	my $url = shift;

	my $wikipedia_str = "wikipedia";

	my $idx = index($url, $wikipedia_str);

	if($idx == -1) {
		return 0;
	}

	return 1;
}
