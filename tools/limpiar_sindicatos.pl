#!/usr/bin/perl

use strict;

my $raw_sindicato_list = "../doc/lista_raw";

open FH, "< $raw_sindicato_list";

print "{\"sindicatos\": [\n";
while(<FH>) {
	my $line = $_;
	$line =~ s/<[^>]*>//gs; #Limpiar tags

	$line =~ s/^\s+//;
	$line =~ s/\s+$//;
	next if(!$line);

	$line =~ s/ +/ /g; #Limpiar mas de un espacio
	$line =~ s/ :/:/g;

	$line =~ s/&aacute;/a/g;
	$line =~ s/&eacute;/e/g;
	$line =~ s/&iacute;/i/g;
	$line =~ s/&oacute;/o/g;
	$line =~ s/&uacute;/u/g;

	my @w = split(/ /, $line);

	$line =~ s/-//g;

	if($w[0] eq "Secretario" || $w[0] eq "Secretaria") {

		if($w[1] eq "General\:") {

			print "\"Nombre secretario\": \"$line\"},\n"; #Imprime secretario
		}
	} else {

		my $comma_pos = where_is_comma($line);

		my $tipo = substr($line, $comma_pos);
		my $nombre = substr($line, 0, $comma_pos);

		$tipo =~ s/, //g;

		print "{\"Nombre sindicato\": \"$tipo $nombre\", "; #Imprime nombre sindicato

	}


}
print "]}\n";

sub where_is_comma
{

	my $line = shift;

	my $comma_pos;
	my $char_pos = -1;

	foreach my $c (split //, $line) {
		$char_pos++;

		if($c eq ',') {
			$comma_pos = $char_pos;
		}
	}

	return $comma_pos;
}
