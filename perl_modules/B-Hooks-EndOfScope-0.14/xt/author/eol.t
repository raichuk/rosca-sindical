use strict;
use warnings;

# this test was generated with Dist::Zilla::Plugin::Test::EOL 0.17

use Test::More 0.88;
use Test::EOL;

my @files = (
    'lib/B/Hooks/EndOfScope.pm',
    'lib/B/Hooks/EndOfScope/PP.pm',
    'lib/B/Hooks/EndOfScope/PP/FieldHash.pm',
    'lib/B/Hooks/EndOfScope/PP/HintHash.pm',
    'lib/B/Hooks/EndOfScope/XS.pm',
    't/00-basic.t',
    't/00-report-prereqs.dd',
    't/00-report-prereqs.t',
    't/01-eval.t',
    't/02-localise.t',
    't/05-exception_xs.t',
    't/06-exception_pp.t',
    't/10-test_without_vm_pure_pp.t',
    't/11-direct_xs.t',
    't/12-direct_pp.t'
);

eol_unix_ok($_, { trailing_whitespace => 1 }) foreach @files;
done_testing;
